// Метод forEach допомагаэ перебрати массив.
//
// Очистити массив повністю можна змінивши його "довжину" на нуль massiv.length = 0.
//
// Перевірить чи є змінна массивом можна за допомого методу isArray() - де в якості парметру функції передається змінна
// яку ми перевірямо.

"use strict"
function filterBy(array, type)  {
    return array.reduce((newArray, currentItem) => {
        if (typeof currentItem != type) {
            newArray.push(currentItem);
        } return newArray
    }, []);
};
return(filterBy(['hello', 'world', 23, '23', null], "string"));