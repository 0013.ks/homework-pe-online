// Метод об'єкту це спосіб зберігання сукопності данних та їх властивостей в одній змінній

// Властовості об'єкта або його ключ може бути тільки такі типи даних як рядок (string), або символи. Значення ключа можуть бути будь-якими.

// Якщо я правильно зрозумів - посилальний тип данних це маєтся на увазі що ми на щось зсилаємось. Об'єкт зберігає в собі ключі та їх значення на які ми зсилаємося коли використувоємо метод об'єкту.
// Якщо я правильно зрозумів - це і є посилальний тип данних.

// function createNewUser() {
//     let name = prompt("What is your name?");
//     let surname = prompt("What is your surname");
//     let newUser = {
//         firstName: name,
//         lastName: surname,
//         login: getLogin(),
//     }
//     function getLogin() {
//         console.log(name[0].toLowerCase() + surname.toLowerCase());
//     }
//     return newUser;
// }
// createNewUser();

function crateNewUser() {
    let name = prompt("What is your name?");
    let surname = prompt("What is your last name?");
    let getLogin = function() {
      return(name[0].toLowerCase() + surname.toLowerCase());
    }
    let newUser = {
        firstName : name,
        lastName :  surname,
        login: getLogin(),
    }
    console.log(newUser);
}
crateNewUser();
