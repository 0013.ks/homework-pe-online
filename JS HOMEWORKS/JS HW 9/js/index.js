    // Cтворити новий HTML елемент можна за допомогою document.createElement.
    //
    //     Перший параметр insertAdjacentHTML означає місце в коді куди вставится html елемент
    // ‘beforebegin’ вставить перед елементом до якого використовується
    // ‘afterbegin’ вставить всередину на початку елемента до якого використовується
    // ‘beforeend’ вставить всередину в кінці елемента до якого використовується
    // ‘afterend’ вставить після елемента до якого використовується
    //
    // видалити елемент можна за допомогою .remove();


const pageHeader = document.querySelector('header');
const list = document.createElement('ul');
pageHeader.prepend(list);
const cities = ["Kiev", "Kharkiv", "Odessa", "Lviv"];

function createCitiesList(cityList, domElem){
    for (city of cities){
        const item = document.createElement('li');
        item.innerHTML = city;
        list.prepend(item);
    }
}
createCitiesList(cities, list);


