const tabsBtn = document.querySelectorAll('.services-tabs-item');
const tabsItems = document.querySelectorAll('.tabs-item');

tabsBtn.forEach(function (item){
        item.addEventListener('click', function (){
           let currentBtn = item;
           let tabId = currentBtn.getAttribute('data-tab');
           let currentTab = document.querySelector(tabId);

           if ( ! currentBtn.classList.contains('active') ){
               tabsBtn.forEach(function (item){
                   item.classList.remove('active');
               });
           }

           tabsBtn.forEach(function (item){
               item.classList.remove('active');
           });

           tabsItems.forEach(function (item){
              item.classList.remove('active');
           });

           currentBtn.classList.add('active');
           currentTab.classList.add('active');
        });
});

function app(){
    const tabs = document.querySelectorAll('.works-tab');
    const worksItems = document.querySelectorAll('.works-item');

    function filter(category, items){
        items.forEach((item) => {
            const isItemFiltered = !    item.classList.contains(category);
            const isShowAll = category.toLowerCase() === 'all'
            if (isItemFiltered && !isShowAll){
                item.classList.add('animation');
            } else {
                item.classList.remove('animation');
            }
        })
    }

    tabs.forEach((tab) => {
        tab.addEventListener('click', () => {
            const currentCategory = tab.dataset.filter;
            filter(currentCategory, worksItems);
        })
    })
}
app()

const button = document.querySelector('.load-button-wrapper')

const contentContainer = document.querySelector('.works-items');

const loadedPhotos = contentContainer.innerHTML;
console.log(loadedPhotos);

const hideWorksItems = document.querySelectorAll('.hide-works-item');

function loadMore(){
    hideWorksItems.forEach((item) =>{
    item.classList.add('visible-works-item');
    button.classList.add('button-hide');
})

}
button.onclick = loadMore;





